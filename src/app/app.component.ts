import { Component, ViewChildren } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Sample Responsive SVG App';
  
  viewState() {
    window.alert(`testing`);
  }
}
