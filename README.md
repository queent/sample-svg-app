# SampleSvgApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.7.


Install Angular CLI in a terminal
`npm install -g @angular/cli`

Navigate to the directory
`cd sample-svg-app`

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
